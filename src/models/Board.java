package models;

import utils.Direction;

import java.util.*;

public class Board {

    private Map<Token,Set<Coordinate>> board;

    public Board(int players) {
        this.init(players);
    }

    private void init(int players) {
        this.board = new HashMap();
        for (int i = 0; i < players; i++){
            this.board.put(Token.values()[i],new HashSet<>());
        }
    }

    public void put(Token token, Coordinate coordinate) {
        this.board.get(token).add(coordinate.clone());
    }

    public void remove(Token token, Coordinate coordinate) {
        this.board.get(token).remove(coordinate);
    }

    public void clear() {
        this.board.clear();
        this.init(Game.NUM_PLAYERS);
    }

    public boolean winExists(Token token) {
        if(this.board.get(token).size() == Coordinate.DIMENSION) {
            Set<Coordinate> coordinates = this.board.get(token);
            Coordinate coordinatesArray[] = coordinates.toArray(new Coordinate[0]);
            if (coordinatesArray[1].direction(coordinatesArray[0]) == coordinatesArray[1].direction(coordinatesArray[2])
                    && coordinatesArray[1].direction(coordinatesArray[2]) != Direction.NON_DIRECTION){ // Refactor pls
                return true;
            }
        }
        return false;
    }

    public Token getToken(Coordinate coordinate) {
        for (Token token : this.board.keySet()) {
            if (this.board.get(token).contains(coordinate)) {
                return token;
            }
        }
        return Token.NONE;
    }

    public boolean isFull(Token token) {
        if(this.board.get(token).size() == Coordinate.DIMENSION) {
            return true;
        }
        return false;
    }

    public boolean positionIsFull(Coordinate coordinate) {
        if(this.getToken(coordinate) == Token.NONE) {
            return false;
        }
        return true;
    }

    public boolean coordExists(Token token, Coordinate coordinate) {
        return this.board.get(token).contains(coordinate);
    }

}
