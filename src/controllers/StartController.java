package controllers;

import models.Game;
import models.State;

public class StartController extends OperationController {

    ColocateControllerBuilder collocateBuilder;

    public StartController(Game game, ColocateControllerBuilder collocateBuilder) {
        super(game);
        this.collocateBuilder = collocateBuilder;
    }

    public void start(int num_players) {
        this.collocateBuilder.build(num_players);
        this.setState(State.IN_GAME);
    }

    @Override
    public void accept(OperationControllerVisitor operationControllerVisitor) {
        operationControllerVisitor.visit(this);
    }
}
