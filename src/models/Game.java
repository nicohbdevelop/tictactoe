package models;

public class Game {

    private Board board;

    private Turn turn;

    private State state;

    public static final int NUM_PLAYERS = 2;

    public Game() {
        board = new Board(NUM_PLAYERS);
        this.setState(State.INIT);
        turn = new Turn();
    }

    public void put(Coordinate coordinate){
        this.board.put(this.turn.getToken(), coordinate);
    }

    public void remove(Coordinate coordinate) {
        this.board.remove(this.turn.getToken(), coordinate);
    }

    public void clear() {
        this.board.clear();
    }

    public boolean coordExists(Coordinate coordinate) {
        return this.board.coordExists(this.turn.getToken(), coordinate);
    }

    public boolean coordIsFull(Coordinate coordinate){
        return this.board.positionIsFull(coordinate);
    }

    public boolean winExists() {
        return this.board.winExists(this.turn.getToken());
    }

    public Token getTurn() {
        return this.turn.getToken();
    }

    public int getIntTurn(){
        return this.turn.get();
    }

    public void changeTurn() {
        this.turn.change();
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState(){
        return this.state;
    }

    public boolean boardIsFull(Token token) {
        return this.board.isFull(token);
    }

    public void initTurn() {
        this.turn.init();
    }

    public Token getToken(Coordinate coordinate) {
        return this.board.getToken(coordinate);
    }


}
