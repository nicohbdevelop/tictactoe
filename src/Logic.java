import controllers.*;
import models.Game;

public class Logic {

    private Game game;

    private StartController startController;

    private ColocateControllerBuilder colocateControllerBuilder;

    private ContinueController continueController;

    public Logic() {
        this.game = new Game();
        colocateControllerBuilder = new ColocateControllerBuilder(game);
        startController = new StartController(game, colocateControllerBuilder);
        continueController = new ContinueController(game);
    }

    public OperationController getController() {
        switch (this.game.getState()){
            case INIT:
                return startController;
            case IN_GAME:
                return colocateControllerBuilder.getColocateController();
            case FINISHED:
                return continueController;
            case EXIT:
                return null;
            default:
                return null;
        }
    }

}
