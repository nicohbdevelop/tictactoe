import controllers.Controller;
import controllers.OperationController;
import views.View;

public class TicTacToe {

    Logic logic;

    View view;

    public TicTacToe(){
        view = new View();
        logic = new Logic();
    }

    private void play() {
        OperationController controller;
        do{
            controller = logic.getController();
            if (controller != null) {
                view.interact(controller);
            }
        }while (controller != null);
    }

    public static void main(String[] args) {
        new TicTacToe().play();
    }

}
