package controllers;

import models.Game;
import models.State;

public class ContinueController extends OperationController {

    public ContinueController(Game game) {
        super(game);
    }

    public void restartGame() {
        this.clearBoard();
        //this.initTurn();
        this.setState(State.INIT);
    }

    public void finish() {
        this.setState(State.EXIT);
    }

    @Override
    public void accept(OperationControllerVisitor operationControllerVisitor) {
        operationControllerVisitor.visit(this);
    }

}
