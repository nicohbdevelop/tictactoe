package utils;

public class LimitedIntInterval {
    private int min;
    private int max;

    public LimitedIntInterval(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public boolean add(int num) {
        return num >= min && num <= max;
    }

}
