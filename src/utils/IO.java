package utils;

import java.util.Scanner;

public class IO {

    private Scanner sc;

    public IO() {
        sc = new Scanner(System.in);
    }

    public void write(String text) {
        System.out.print(text);
    }

    public void writeln(String text) {
        System.out.println(text);
    }

    public int readInt() {
        return Integer.parseInt(sc.nextLine());
    }

    public String readString() {
        return sc.nextLine();
    }


}
