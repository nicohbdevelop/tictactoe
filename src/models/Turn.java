package models;

public class Turn {

    private int turn;

    public Turn() {
        this.init();
    }

    public void init() {
        this.turn = 0;
    }

    public void change(){
        turn = turn ^ 1;
    }

    public Token getToken(){
        return Token.values()[turn];
    }

    public int get(){
        return this.turn;
    }

}
