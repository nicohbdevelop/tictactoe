package controllers;
import models.Game;

public class ColocateControllerBuilder {

    private Game game;

    private ColocateController colocateController[][];

    private final int moveControllerPosition = 0;

    private final int putControllerPosition = 1;

    public ColocateControllerBuilder(Game game) {
        this.game = game;
        colocateController = new ColocateController[this.game.NUM_PLAYERS][2];
    }

    public void build(int users) {
        CoordinateController coordinateController[][] = new CoordinateController[this.game.NUM_PLAYERS][2];
        for (int i = 0; i < game.NUM_PLAYERS; i++) {
            for (int j = 0; j < 2; j++) {
                if (i < users) {
                    coordinateController[i][j] = new ManualCoordinateController(game);
                } else {
                    coordinateController[i][j] = new RandomCoordinateController(game);
                }
            }
        }

        for (int i = 0; i < game.NUM_PLAYERS; i++) {
            for(int j = 0; j < 2; j++) {
                if (j == moveControllerPosition) {
                    colocateController[i][j] = new MoveController(game, coordinateController[i][j]);
                }else {
                    colocateController[i][j] = new PutController(game, coordinateController[i][j]);
                }

            }
        }
    }

    public ColocateController getColocateController() {
        if (this.game.boardIsFull(game.getTurn())) {
            MoveController moveController = (MoveController) colocateController[this.game.getIntTurn()][moveControllerPosition];
            return moveController;
        }
        PutController putController = (PutController) colocateController[this.game.getIntTurn()][putControllerPosition];
        return putController;
    }

}
