package views;

import controllers.*;

public class View implements OperationControllerVisitor {

    StartView startView;

    GameView gameView;

    public View() {
        startView = new StartView();
        gameView = new GameView();
    }

    public void interact(OperationController operationController) {
      operationController.accept(this);
    }

    @Override
    public void visit(StartController startController) {
        StartView startView = new StartView();
        startView.interact(startController);
    }

    @Override
    public void visit(ColocateController colocateController) {
        GameView gameView = new GameView();
        gameView.interact(colocateController);
    }

    @Override
    public void visit(ContinueController continueController) {
        ContinueView continueView = new ContinueView();
        continueView.interact(continueController);
    }
}
