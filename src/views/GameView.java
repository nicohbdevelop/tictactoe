package views;

import controllers.*;
import models.Coordinate;
import utils.IO;
import utils.YesNoDialog;

public class GameView implements ColocateControllerVisitor {

    IO io = new IO();

    public void interact(ColocateController colocateController) {
        colocateController.accept(this);
    }

    @Override
    public void visit(PutController putController) {
        CoordinateView coordinateView = new CoordinateView();
        Coordinate target = null;
        TokenView tokenView = new TokenView(putController.getIntTurn());
        io.writeln("");
        io.writeln("--- Turno de " + String.valueOf(tokenView.getToken()) + "---");

        if(putController.getCoordinateController() instanceof ManualCoordinateController){
            do{
                target = coordinateView.read("Introduce la coordenada de destiono");
            }while (!putController.validateTarget(target));
        }else if(putController.getCoordinateController() instanceof RandomCoordinateController) {
            target = putController.getCoordinateController().getTarget();
            io.writeln("La maquina ha introducido la coordenada: fila: " + (target.getRow()+1) + " columna: " + (target.getColumn()+1));
        }

        putController.put(target);
        new BoardView().write(putController);
        if(putController.winExists()){
            tokenView.writeWinner();
        }
    }

    @Override
    public void visit(MoveController moveController) {
        CoordinateView coordinateView = new CoordinateView();
        Coordinate origin = null;
        Coordinate target = null;
        TokenView tokenView = new TokenView(moveController.getIntTurn());
        io.writeln("--- Turno de " + tokenView.getToken());

        if(moveController.getCoordinateController() instanceof ManualCoordinateController){
            do{
                origin = coordinateView.read("Introduce la coordenada de origen");
                target = coordinateView.read("Introduce la coordenada de destiono");
            }while (!moveController.validateOrirgin(origin) || !moveController.validateTarget(target));
        }else if(moveController.getCoordinateController() instanceof RandomCoordinateController) {
            origin = moveController.getCoordinateController().getOrigin();
            target = moveController.getCoordinateController().getTarget();
            io.writeln("La maquina ha introducido la coordenada: fila: " + (target.getRow()+1) + " columna: " + (target.getColumn()+1));
        }

        moveController.move(origin, target);
        new BoardView().write(moveController);
        if(moveController.winExists()){
            tokenView.writeWinner();
        }
    }


}
