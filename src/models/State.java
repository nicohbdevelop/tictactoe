package models;

public enum State {
    INIT,
    IN_GAME,
    FINISHED,
    EXIT
}
