package views;

import controllers.StartController;
import utils.LimitedIntDialog;

public class StartView {

    public void interact(StartController startController) {
        LimitedIntDialog limitedIntDialog = new LimitedIntDialog("Introduce el nº de jugadores",0,2);
        int num_players = limitedIntDialog.read();
        startController.start(num_players);
    }

}
