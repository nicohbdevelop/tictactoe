package controllers;

import models.Coordinate;
import models.Game;

public class RandomCoordinateController extends CoordinateController {

    public RandomCoordinateController(Game game) {
        super(game);
    }

    @Override
    public Coordinate getOrigin() {
        Coordinate coordinate = new Coordinate();
        boolean ok;
        do{
            coordinate.random();
            ok = super.checkOrigin(coordinate);
        }while (!ok);
        return coordinate;
    }

    @Override
    public Coordinate getTarget() {
        Coordinate coordinate = new Coordinate();
        boolean ok;
        do {
            coordinate.random();
            ok = !super.coordIsFull(coordinate);
        } while (!ok);
        return coordinate;
    }
}
