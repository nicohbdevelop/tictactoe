package controllers;


import models.Coordinate;
import models.Game;

public abstract class ColocateController extends OperationController {

    CoordinateController coordinateController;

    public ColocateController(Game game, CoordinateController coordinateController) {
        super(game);
        this.coordinateController = coordinateController;
    }

    public boolean validateTarget(Coordinate coordinate) {
        if (super.coordIsFull(coordinate)){
            return false;
        }
        return true;
    }

    public CoordinateController getCoordinateController() {
        return coordinateController;
    }

    public abstract void accept(ColocateControllerVisitor colocateControllerVisitor);
}
