package controllers;

import models.Coordinate;
import models.Game;

public class PutController extends ColocateController {

    public PutController(Game game, CoordinateController coordinateController) {
        super(game, coordinateController);
    }

    public void put(Coordinate target) {
        super.put(target);
    }

    @Override
    public void accept(OperationControllerVisitor operationControllerVisitor) {
        operationControllerVisitor.visit(this);
    }

    @Override
    public void accept(ColocateControllerVisitor colocateControllerVisitor) {
        colocateControllerVisitor.visit(this);
    }
}
