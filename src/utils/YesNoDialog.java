package utils;

public class YesNoDialog {

    private String title;

    String resp;

    public YesNoDialog(String title) {
        this.title = title;
    }

    public boolean read() {
        IO io = new IO();
        do {
            io.write(title + " (yes/no)");
            resp = io.readString();
        }while (!resp.equals("yes") && !resp.equals("no"));

        return resp.equals("yes") || resp.equals("Yes");
    }

}
