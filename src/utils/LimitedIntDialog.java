package utils;

public class LimitedIntDialog {

    private String title;

    LimitedIntInterval limitedIntInterval;

    public LimitedIntDialog(String title, int min, int max){
        this.title = title;
        limitedIntInterval = new LimitedIntInterval(min, max);
    }

    public int read(){
        IO io = new IO();
        io.writeln(title);
        int num;
        do{
            num = io.readInt();
        }while (!this.limitedIntInterval.add(num));
        return num;
    }

}
