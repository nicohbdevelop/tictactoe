package views;

import models.Token;
import utils.IO;

public class TokenView {

    public final char[] TOKENS = {'X' , 'O', '-'};

    int token;

    Token tokenView;

    public TokenView(Token token){
        this.tokenView = token;
    }

    public TokenView(int token) {
        this.token = token;
    }

    public void writeToken() {
        IO io = new IO();
        io.write(" " + TOKENS[this.token] + " ");
    }

    public char getToken() {
        return TOKENS[this.token];
    }

    public void writeWinner() {
        IO io = new IO();
        io.writeln("El jugador " + TOKENS[token] + " ha ganado la partida!!!!!");
    }

}
