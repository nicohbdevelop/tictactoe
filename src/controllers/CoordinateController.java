package controllers;

import models.Coordinate;
import models.Game;

public abstract class CoordinateController extends Controller {

    public CoordinateController(Game game) {
        super(game);
    }

    public abstract Coordinate getOrigin();

    public abstract Coordinate getTarget();

}
