package controllers;

import models.Coordinate;
import models.Game;
import models.State;
import models.Token;

public class Controller {

    private Game game; // encapsulation bro

    public Controller(Game game) {
        this.game = game;
    }

    public void put(Coordinate coordinate) {
        this.game.put(coordinate);
        if(this.game.winExists()) {
            this.game.setState(State.FINISHED);
        }else{
            this.game.changeTurn();
        }
    }

    public void remove(Coordinate coordinate) {
        this.game.remove(coordinate);
    }

    public boolean boardIsFull(Token token) {
        return this.game.boardIsFull(token);
    }

    public boolean winExists() {
        return this.game.winExists();
    }

    public void setState(State state) {
        this.game.setState(state);
    }

    public State getState() {
        return this.game.getState();
    }

    public boolean checkOrigin(Coordinate coordinate) {
        return this.game.coordExists(coordinate);
    }

    public boolean coordIsFull(Coordinate coordinate) {
        return this.game.coordIsFull(coordinate);
    }

    public int getIntTurn(){
        return this.game.getIntTurn();
    }

    public Token getTurn(){
        return this.game.getTurn();
    }

    public void clearBoard() {
        this.game.clear();
    }

    public void initTurn() {
        this.game.initTurn();
    }

    public Token getToken(Coordinate coordinate) {
        return this.game.getToken(coordinate);
    }

}
