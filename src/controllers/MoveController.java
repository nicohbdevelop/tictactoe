package controllers;

import models.Coordinate;
import models.Game;

public class MoveController extends ColocateController {

    public MoveController(Game game, CoordinateController coordinateController) {
        super(game, coordinateController);
    }

    public void move(Coordinate origin, Coordinate target) {
        super.remove(origin);
        super.put(target);
    }

    public boolean validateOrirgin(Coordinate origin) {
        return super.checkOrigin(origin);
    }

    @Override
    public void accept(OperationControllerVisitor operationControllerVisitor) {
        operationControllerVisitor.visit(this);
    }

    @Override
    public void accept(ColocateControllerVisitor colocateControllerVisitor) {
        colocateControllerVisitor.visit(this);
    }
}
