package utils;

public enum Direction {
    HORIZONTAL,
    VERTICAL,
    DIAGONAL,
    INVERSE,
    NON_DIRECTION
}
