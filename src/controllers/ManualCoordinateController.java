package controllers;

import models.Coordinate;
import models.Game;

public class ManualCoordinateController extends CoordinateController {

    public ManualCoordinateController(Game game) {
        super(game);
    }

    @Override
    public Coordinate getOrigin() {
        return new Coordinate();
    }

    @Override
    public Coordinate getTarget() {
        return new Coordinate();
    }
}
