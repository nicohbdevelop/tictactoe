package controllers;

public interface ColocateControllerVisitor {

    void visit(MoveController moveController);

    void visit(PutController putController);
}
