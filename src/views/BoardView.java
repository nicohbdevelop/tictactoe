package views;
import controllers.Controller;
import models.Board;
import models.Coordinate;
import models.Token;
import utils.IO;

public class BoardView {

    public void write(Controller controller) {
        IO io = new IO();


        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                io.write(" " + String.valueOf(controller.getToken(new Coordinate(i, j))) + " ");
            }
            io.writeln("");
        }
    }

}
