package views;

import controllers.ContinueController;
import utils.IO;
import utils.YesNoDialog;

public class ContinueView {

    public void interact(ContinueController continueController) {
        YesNoDialog yesNoDialog = new YesNoDialog("¿Deseas jugar otra partida?");
        boolean stillPlaying = yesNoDialog.read();
        if(stillPlaying) {
            continueController.restartGame();
        }else{
            IO io = new IO();
            io.writeln("La partida ha finalizado!");
            continueController.finish();
        }

    }
}
