package views;

import models.Coordinate;
import utils.IO;

public class CoordinateView {

    public Coordinate read(String title){
        IO io = new IO();
        int row, col;
        do {
            io.writeln(title);
            io.write("Fila: ");
            row = io.readInt();
            io.write("Columna: ");
            col = io.readInt();
        }while((row > 3 || row < 1) || (col > 3 || col < 1));
        Coordinate coordinate = new Coordinate(row-1,col-1);
        return coordinate;
    }


}
